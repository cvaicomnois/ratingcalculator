/*
 * Descricao da classe Contestant
 *
 *
 *
 */
#include <string>

#ifndef __contestant__
#define __contestant__

class Contestant
{
    public:
        double seed;
        double points;
        int rank;
        int rating;
        int needRating;
        int delta;
        std::string name;

        Contestant(
                std::string name,
                int rank,
                double points,
                int rating
                );
};

#endif
