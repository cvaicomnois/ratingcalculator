/*
 * Descricao da classe RatingCalculator
 *
 *
 *
 */
#include <vector>
#include "Contestant.hpp"

#ifndef __ratingcalculator__
#define __ratingcalculator__

#define INFLATION_CALC 1
#define INITIAL_RATING 1500

class RatingCalculator
{
    public:
        static double getEloWinProbability (double ra, double rb);

        static double getEloWinProbability (Contestant a, Contestant b);

        static double getSeed (std::vector<Contestant> contestants, int rating);

        static int getRatingToRank (std::vector<Contestant> contestants, double rank);

        static void process(std::vector<Contestant> &contestants);
};

#endif

