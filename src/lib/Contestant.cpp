/*
 * Implementacao dos metodos da classe Contestant.
 *
 *
 *
 * last modified: 
 * created: lucas
 */
#include "Contestant.hpp"
#include <string>

Contestant::Contestant(std::string name, int rank, double points, int rating) :
    name(name),
    seed(0.0),
    rank(rank),
    points(points),
    rating(rating),
    delta(0.0)
{

}
