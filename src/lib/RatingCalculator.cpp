/*
 * Implementacao dos metodos da classe RatingCalculator.
 *
 *
 *
 * last modified: 
 * created: lucas
 */
#include <algorithm>
#include <vector>
#include <cmath>
#include "RatingCalculator.hpp"
#include "Contestant.hpp"

static bool cmp(Contestant a, Contestant b)
{
    return a.rating < b.rating;
}

double RatingCalculator::getEloWinProbability (double ra, double rb)
{
	return 1.0 / (1 + pow(10, (rb - ra) / 400.0));
}

double RatingCalculator::getEloWinProbability (Contestant a, Contestant b)
{
	return getEloWinProbability(a.rating, b.rating);
}

double RatingCalculator::getSeed (std::vector<Contestant> contestants, int rating)
{
	Contestant extraContestant("", 0, 0, rating);

	double result = 1;
	for (Contestant other : contestants)
		result += getEloWinProbability (other, extraContestant);
	
	return result;
}

int RatingCalculator::getRatingToRank (std::vector<Contestant> contestants, double rank)
{
	int left = 1;
	int right = 8000;
	
	while (right - left > 1)
	{
		int mid = (left + right) / 2;

		if (getSeed(contestants, mid) < rank)
			right = mid;
		else
			left = mid;
	}

	return left;
}

void RatingCalculator::process(std::vector<Contestant> &contestants)
{
    for (auto &a : contestants)
    {
        a.seed = 1;
        
        for (auto &b : contestants)
            if (a.name != b.name)
                a.seed += getEloWinProbability(b, a);
    }

    for (Contestant &c : contestants)
    {
        double midRank = sqrt(((double) c.rank) * c.seed);
        
        c.needRating = getRatingToRank(contestants, midRank);
        c.delta = (c.needRating - c.rating) / 2;
    }

#if (INFLATION_CALC == 1)

    sort(contestants.rbegin(), contestants.rend(), cmp);

    int sum = 0;
    for (auto c : contestants)
        sum += c.delta;
    
    int inc  = (-sum / (int)contestants.size()) - 1;
    for (auto &c : contestants){
        c.delta += inc;
    }

    sum = 0;
    int zeroSumCount;
    zeroSumCount = std::min(
            (int) (4.0 * round(sqrt((double)contestants.size()))),
            (int)contestants.size());

    for (int i = 0; i < zeroSumCount; i++)
        sum += contestants[i].delta;

    inc = std::min(std::max(-sum/zeroSumCount, -10), 0);
    
    for (auto &c : contestants)
        c.delta += inc;

#endif

}
