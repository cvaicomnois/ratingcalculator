/*
 * Refaz o ranking da galera
 * compilar e guardar localmente em ./bin
 * ex:
 * 
 * Para rodar os rounds de 1 a 8:
 * 
 * ** na pasta raiz do repositorio:
 * 
 *       $ ./bin/run_rating_system 1 8
 */

#include <cstdio>
#include <cstdlib>

using namespace std;

int run_system_test(int round)
{
    char cmd[1000];

    sprintf (cmd, "./bin/calculator files/round_%d.rank files/round_%d.score > files/round_%d.rank 2> files/round_%d.diff", round-1,
            round, round, round);

    int res = system(cmd);

    printf ("%d: %d\ncmd: %s\n\n", round, res, cmd);

    return res;
}

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        puts ("Parametros invalidos!");
        return -1;
    }

    char cmd[1000];
    int start, end;

    sscanf(argv[1], "%d", &start);
    sscanf(argv[2], "%d", &end);

    for (;start <= end; start++)
    {
        printf ("#### Processando Round %d ####\n", start);
        run_system_test(start);
    }
    
    return 0;
}

