/*
 *
 *
 * last modified: 
 * created: lucas
 */
#include <cstdlib>
#include <iostream>
#include <cstdio>
#include <algorithm>
#include <vector>
#include <map>
#include "RatingCalculator.hpp"
#include "Contestant.hpp"

using namespace std;

typedef map<string, int> User;

int getUsers(const char *userFile, User &users);
int getContestants(const char *finalScore, User &users, vector<Contestant> &contestants);

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        fprintf (stderr, "Erro: Parametros invalidos!\n\n");
        fprintf (stderr, "arg1: Ranking Atual\n"
                "arg2: Posicoes do Round\n");

        return -1;
    }

    //======================
    //      Welcome!
    //======================
    
    User users;
    vector <Contestant> contestants;
    
    RatingCalculator RC;

    fprintf (stderr, "Users in Rank: %d\n",
            getUsers(argv[1], users));

    fprintf (stderr, "Contestants in this Round: %d\n",
            getContestants(argv[2], users, contestants));

    //=====================
    //Get rating changes
    //=====================
    
    RC.process(contestants);

    //======================
    //Update changes
    //======================
    fprintf(stderr, "Rating changes are out!\n\n");

    for (auto c : contestants)
    {
    	fprintf(stderr, "%s\n%d -> ", c.name.c_str(), c.rating);
        users[c.name] = c.rating + c.delta;
        fprintf(stderr, "%d (%+d)\n\n", users[c.name], c.delta);
    }

    //=======================
    //Output the new rank
    //=======================
    
    vector < pair <int, string> > rank;
    for (auto user : users)
        rank.emplace_back(make_pair(user.second, user.first));

    sort(rank.rbegin(), rank.rend());

    for (auto u : rank)
        printf ("%s %d\n", u.second.c_str(), u.first);

    //========================
    //          bye!
    //========================

    return 0;
}

int getUsers(const char *usersFile, User &users)
{
    FILE *fp = fopen(usersFile, "r");

    if (fp == NULL)
    {
        fprintf (stderr, "Users File: erro ao ler %s\n", usersFile);
        return 0;
    }

    int rating;
    char name[200];

    while (EOF != fscanf(fp, "%s %d", name, &rating))
        users[name] = rating;
    
    return (int) users.size();
}

int getContestants(const char *finalScore, User &users, vector<Contestant> &contestants)
{
    FILE *fp = fopen(finalScore, "r");

    if (fp == NULL)
    {
        fprintf(stderr, "Score File: erro ao ler %s\n", finalScore);
        return 0;
    }

    char name[200];

    while (EOF != fscanf(fp, "%s", name))
    {
        int previousRating = users[name];

        contestants.push_back(
                Contestant(
                    name,
                    (int) contestants.size() + 1,
                    0,
                    previousRating ? previousRating : INITIAL_RATING)
                );
    }

    return (int) contestants.size();
}

